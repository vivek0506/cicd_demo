export FIREBASE_TOKEN=$FIREBASE_TOKEN

# Needed for env variable changes
./gradlew --stop

# Build and deploy apk to Firebase App Distribution
./gradlew assembleRelease appDistributionUploadRelease