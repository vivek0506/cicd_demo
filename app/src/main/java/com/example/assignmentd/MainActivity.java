package com.example.assignmentd;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.google.gson.Gson;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {
    private Context mContext;
    private RecyclerView rvProduct;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        rvProduct = findViewById(R.id.rvProductList);
        parseJson();
    }
    public void parseJson(){
        InputStream inputStream = mContext.getResources().openRawResource(R.raw.product_list);
        String jsonString = new Scanner(inputStream).useDelimiter("\\A").next();
         try {
             ProductResponse data = new Gson().fromJson(jsonString, ProductResponse.class);
             setAdapter(data.getItems().getProductList());
         }catch (Exception e){
             Log.e("VIVEK", "parseJson: "+e.getLocalizedMessage());
         }

    }

    private void setAdapter(ArrayList<Product> productList) {
        ProductListAdapter productListAdapter = new ProductListAdapter(productList,mContext);
        rvProduct.setAdapter(productListAdapter);
        rvProduct.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

    }

}