package com.example.assignmentd;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.ArrayList;
public class ProductResponse {
    @SerializedName("items")
    @Expose
    private Items items;

    public Items getItems() {
        return items;
    }

    public void setItems(Items items) {
        this.items = items;
    }

}

class Items {

    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("product_list")
    @Expose
    private ArrayList<Product> productList = null;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }

}
