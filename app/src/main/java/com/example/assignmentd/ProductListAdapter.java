package com.example.assignmentd;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {
    private ArrayList<Product> products;
    private Context mContext;
    public ProductListAdapter(ArrayList<Product> products, Context mContext) {
        this.products = products;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ProductListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list,parent,false);
        return new ProductListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListAdapter.ViewHolder holder, int position) {
        holder.tvProducctName.setText(products.get(position).getTitle());
        holder.tvPrice.setText(products.get(position).getOriginalPrice());
        holder.tvWeight.setText(products.get(position).getQuantity());

        Glide.with(mContext)
                .load(products.get(position).getImageUrl())
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
         if(products != null && products.size() > 0){
             return products.size();
         }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
         private final ImageView imageView;
         private final TextView tvProducctName;
         private final TextView tvWeight;
         private final TextView tvPrice;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.ivProductImage);
            tvProducctName = itemView.findViewById(R.id.tvProductName);
            tvWeight = itemView.findViewById(R.id.tvProductWeight);
            tvPrice = itemView.findViewById(R.id.tvProductPrice);
        }
    }
}
